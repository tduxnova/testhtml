import React from 'react';
import styled from 'styled-components';


import { store } from '../App';
import { makeWindowActive } from '../redux/actions';


const Box = styled.div `
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  justify-content: space-between;
  height: 165px;
  cursor: pointer;

  @media (max-width: 800px) {
    margin: 20px;
  }
`;

const WindowImg = styled.img `
  margin-bottom: 10px;
`;

const SelectNomination = styled.span `
  font-size: 13px;
  font-weight: bold;
  color: ${(props) => props.active ? '#0099de' : 'black'};
  text-decoration: ${(props) => props.active ? 'underline' : 'none'};
`;


const Window = (props) => {
  const { windowData } = props;

  function setActive() {
    store.dispatch(makeWindowActive(windowData.id));
  }

  return (
    <Box onClick={setActive}>
      <WindowImg 
        src={require(`../images/${windowData.imgName}`)} 
        alt={windowData.name} 
        width={windowData.minWidth} 
        height={windowData.minHeight} 
      />

      <SelectNomination 
        active={windowData.active}
        // onClick={setActive}
      > 
        {windowData.name} 
      </SelectNomination>
    </Box>
  );
};

export default Window;
