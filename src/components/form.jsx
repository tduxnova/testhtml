import React from 'react';
import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';


import bg from '../images/bg.png';


const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const Box = styled.div `
  width: 100%;
  height: 475px;
  background: url(${bg}) no-repeat;
  background-size: cover;
`;

const Content = styled.div `
  max-width: 1000px;
  height: 100%;
  margin: 0 auto;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
`;

const Block = styled.div `
  width: fit-content;
  display: flex;
  flex-flow: column;
  align-items: center;
  margin-left: 100px;

  @media (max-width: 500px) {
    width: 100%;
    justify-content: center;
    margin-left: 0;
  }
`;

const Title = styled.span `
  text-transform: uppercase;
  font-weight: bold;
  font-size: 16px;
  color: #9da3d7;
`;

const FormBox = styled.div `
  width: 230px;
  height: 170px;
  background: #9da3d7;
  margin: 10px 0;
  padding: 10px 0;
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  justify-content: space-evenly;
`;

const FormTitle = styled.span `
  color: white;
  text-transform: uppercase;
  text-align: center;
  font-size: 14px;
`;

const PhoneInput = styled.input `
  border: none;
  padding: 5px 8px;
  outline: none;

  &::placeholder {
    font-weight: 500;
  }
`;

const Btn = styled.button `
  border: none;
  outline: none;
  text-transform: uppercase;
  text-align: center;
  font-weight: bold;
  color: black;
  background: #ffc087;
  width: 160px;
  height: 28px;
  cursor: pointer;
`;


const Form = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [phone, setPhone] = React.useState('');

  const handleOpen = () => {
    if (/^\+?[0-9]+$/gim.test(phone)) {
      setOpen(true);
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Box>
      <Content>
        <Block>
          <Title> Форма с кнопкой </Title>

          <FormBox>
            <FormTitle> Оставь телефон и получи что-то офигенное </FormTitle>
            <PhoneInput 
              placeholder='Введите номер'
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
            />
            <Btn onClick={handleOpen}> Получить </Btn>
          </FormBox>
        </Block>
      </Content>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >

        <Fade in={open}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title"> Сообщение отправлено </h2>
          </div>
        </Fade>
      </Modal>
    </Box>
  );
};

export default Form;
