import React from 'react';
import styled from 'styled-components';


import logo from '../images/logo.png';


const Box = styled.div `
  width: 100%;
  background: #dcdcdc;
`;

const Content = styled.div `
  max-width: 1000px;
  margin: 0 auto;
  padding: 60px 0;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;

  @media (max-width: 1000px) {
    padding: 60px 50px;
  }

  @media (max-width: 610px) {
    justify-content: center;
    padding: 40px 50px;
  }
`;

const LogoBox = styled.div `
  display: flex;
  align-items: center;
  width: 255px;

  @media (max-width: 610px) {
    margin-bottom: 15px;
  }
`;

const LogoImg = styled.img `
  width: 100px;
  height: 100px;
  margin-right: 10px;
`;

const LogoText = styled.div `
  display: flex;
  flex-flow: column nowrap;
`;

const Title = styled.h2 `
  margin-bottom: 10px;
`;

const Subtitle = styled.span `
  font-size: 13px;
`;

const PhoneBox = styled(LogoText) `
  justify-content: center;
`;

const Phone = styled.a `
  font-size: 30px;
  margin-bottom: 5px;
  outline: none;

  @media (max-width: 610px) {
    font-size: 25px;
  }
`;


const Header = () => {
  const phones = ['+7(499) 777-77-77', '+7(499) 777-77-77'];

  const phonesView = phones.map((phone, index) => {
    return (
      <Phone href={`tel:${phone}`} key={index}> {phone} </Phone>
    );
  });
  
  return (
    <Box>
      <Content>
        <LogoBox>
          <LogoImg src={logo} alt='logo' />
          <LogoText>
            <Title> Название компании </Title>
            <Subtitle> самая клевая компания </Subtitle>
          </LogoText>
        </LogoBox>

        <PhoneBox>
          {phonesView}
        </PhoneBox>
      </Content>
    </Box>
  );
};

export default Header;
