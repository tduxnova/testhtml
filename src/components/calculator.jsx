import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';


import arrow from '../images/arrow.png';
import user from '../images/user.png';
import Window from './window';


const Content = styled.div `
  max-width: 1000px;
  margin: 0 auto;

  @media (max-width: 1000px) {
    padding: 0 50px;
  }
`;

const Title = styled.p `
  color: #9da3d7;
  text-align: center;
  text-transform: uppercase;
  font-weight: bold;
  font-size: 25px;
  margin-bottom: 40px;

  @media (max-width: 780px) {
    font-size: 20px;
  }
`;

const WindowsBox = styled.div `
  display: flex;
  flex-flow: row wrap;
  width: 100%;
  justify-content: space-between;
  margin-bottom: 50px;
  padding: 0 10px;

  @media (max-width: 800px) {
    justify-content: center;
  }
`;

const CalculatorBox = styled.div `
  width: 100%;
  padding: 30px 10px;
  border: 2px solid #d6d6d6;
  border-radius: 20px;
  box-shadow: 3px 3px 15px rgba(16,39,71,0.5);
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 80px;
`;

const CriteriasBox = styled.div `
  width: 100%;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  margin-bottom: 60px;

  @media (max-width: 800px) {
    flex-flow: column wrap;
    align-items: center;
    width: 80%;
  }
`;

const SizesBox = styled.div `
  width: 40%;
  display: flex;
  flex-flow: column nowrap;
  align-items: center;

  @media (max-width: 800px) {
    width: 100%;
  }
`;

const WidthCriterion = styled.span `
  font-style: italic;
  font-weight: bold;
  font-size: 18px;
  margin-bottom: 20px;
`;

const HeightWindow = styled.div `
  display: flex;
  align-items: initial;
  margin-bottom: 30px;
  width: 310px;
`;

const HeightCriterion = styled(WidthCriterion) `
  transform: rotate(-90deg);
  padding-top: 20px;
  font-size: 18px;
  margin: 0;

  @media (max-width: 1000px) {
    padding-top: 55px;
  }

  @media (max-width: 400px) {
    padding-top: 70px;
  }
`;

const Img = styled.img `
  margin-left: ${(props) => props.marginLeft ? props.marginLeft : '0'};
`;

const Parameters = styled.div `
  display: flex;
  flex-flow: row wrap;
  align-items: flex-end;

  @media (max-width: 445px) {
    flex-flow: column;
    align-items: center;
  }
`;

const InputBox = styled.div `
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
`;

const Text = styled.span `
  color: black;
  font-size: 20px;
`;

const Input = styled.input `
  width: 85px;
  height: 45px;
  border: 2px solid #a2a2a2;
  border-radius: 10px;
  outline: none;
  text-align: center;
  font-size: 30px;
  margin-top: 10px;
  color: #5d5d5d;
`;

const Cross = styled.span `
  color: #0099de;
  font-size: 35px;
  font-weight: bold;
  margin: 0 15px;
`;

const ChoiceBox = styled(SizesBox) `
  width: 50%;
  align-items: baseline;

  @media (max-width: 800px) {
    margin-top: 25px;
    width: 70%;
  }

  @media (max-width: 630px) {
    width: 100%;
  }
`;

const Subtitle = styled.h3 `
  color: #0099de;
  font-size: 25px;
  margin: 15px 0 50px;

  @media (max-width: 1000px) {
    margin: 15px 0 42px;
  }
`;

const HeadLine = styled.span `
  color: black;
  font-weight: bold;
  font-size: 20px;
  margin-bottom: 28px;
`;

const RadioBtnBox = styled.div `
  display: flex;
  flex-flow: row wrap;
  margin-bottom: 50px;

  @media (max-width: 1000px) {
    margin-bottom: 20px;
  }
`;

const BtnBox = styled.div `
  display: flex;
  align-items: center;
  margin-right: 15px;

  @media (max-width: 1000px) {
    margin-bottom: 10px;
  }
`;

const RadioBtn = styled.input `
  border: 2px solid #a2a2a2;
  width: 25px;
  height: 15px;
  cursor: pointer;
`;

const Item = styled.label `
  color: black;
  font-size: 15px;
  cursor: pointer;
`;

const Select = styled.select `
  width: 335px;
  height: 45px;
  border: 2px solid #a2a2a2;
  border-radius: 10px;
  outline: none;
  color: black;
  padding: 0 15px;
  cursor: pointer;
  appearance: none;

  @media (max-width: 630px) {
    width: 45vw;
  }
`;

const Arrow = styled.img `
  position: absolute;
  right: 20px;
  top: 12px;
  cursor: pointer;
`;

const Btns = styled.div `
  width: 495px;
  margin: 0 auto;
  display: flex;
  align-items: center;

  @media (max-width: 630px) {
    flex-flow: column wrap;
    width: 100%;
  }
`;

const Icon = styled.div `
  background: white;
  width: 41px;
  height: 41px;
  border-radius: 10px;
  position: absolute;
  top: 12px;
  left: 1px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const User = styled.img `
  width: 80%;
  height: 80%;
`;

const NumberInput = styled(Input) `
  background: #c6c6c6;
  color: black;
  font-size: 15px;
  width: 235px;
  padding-right: 20px;
  border: none;

  @media (max-width: 370px) {
    width: 200px;
    padding-right: 0;
  }
`;

const CalculateBtn = styled.button `
  text-transform: uppercase;
  width: 235px;
  height: 45px;
  border-radius: 10px;
  text-align: center;
  color: white;
  background: #9da3d7;
  outline: none;
  border: none;
  cursor: pointer;
  font-weight: bold;
  font-size: 15px;
  margin: 10px 0 0 25px;

  @media (max-width: 630px) {
    margin: 10px 0;
  }

  @media (max-width: 370px) {
    width: 200px;
  }
`;


const Calculator = (props) => {
  const { windows } = props;

  const windowsView = windows.map((window) => {
    return (
      <Window windowData={window} key={window.id} />
    );
  });

  const activeWindow = windows.find((window) => window.active);

  return (
    <Content>
      <Title> Калькулятор <br /> (стоимость пользователь узнает по телефону) </Title>

      <WindowsBox>
        {windowsView}
      </WindowsBox>

      <CalculatorBox>
        <CriteriasBox>
          <SizesBox>
            <WidthCriterion> ширина </WidthCriterion>

            <HeightWindow>
              <HeightCriterion> высота </HeightCriterion>
              <Img 
                src={require(`../images/${activeWindow.imgName}`)} 
                alt={activeWindow.name}
                width={activeWindow.maxWidth}
                height={activeWindow.maxHeight}
                marginLeft={activeWindow.marginLeft}
              />
            </HeightWindow>

            <Parameters>
              <InputBox>
                <Text> ширина, см </Text>
                <Input />
              </InputBox>

              <Cross> x </Cross>

              <InputBox>
                <Text> высота, см </Text>
                <Input />
              </InputBox>
            </Parameters>
          </SizesBox>

          <ChoiceBox>
            <Subtitle> { [ activeWindow.name, activeWindow.short ? '' : 'окно KBE'].join(' ') } </Subtitle>

            <HeadLine> 1. Критерий выбора </HeadLine>

            <RadioBtnBox>
              <BtnBox>
                <RadioBtn type='radio' id='first' name='choice' value='100' />
                <Item htmlFor='first'> пункт выбора 1 </Item>
              </BtnBox>

              <BtnBox>
                <RadioBtn type='radio' id='second' name='choice' value='150' />
                <Item htmlFor='second'> пункт выбора 2 </Item>
              </BtnBox>

              <BtnBox>
                <RadioBtn type='radio' id='third' name='choice' value='200' />
                <Item htmlFor='third'> пункт выбора 3 </Item>
              </BtnBox>
            </RadioBtnBox>

            <HeadLine> 2. Выберите открывание створки </HeadLine>

            <div style={{ position: 'relative' }}>
              <Select>
                <option> Не открывается </option>
                <option> Открывается </option>
              </Select>

              <Arrow src={arrow} alt='arrow' />
            </div>
          </ChoiceBox>
        </CriteriasBox>

        <Btns>
          <div style={{ position: 'relative' }}>
            <Icon>
              <User src={user} alt='user' />
            </Icon>

            <NumberInput placeholder='Введите номер' />
          </div>

          <CalculateBtn> Рассчитать </CalculateBtn>
        </Btns>
      </CalculatorBox>
    </Content>
  );
};

const mapStateToProps = (state) => {
  return  { windows: state.windows };
};

export default connect(mapStateToProps)(Calculator);
