import React from 'react';
import styled from 'styled-components';
import Slider from "react-slick";


import slide1 from '../images/slide1.png';
import slide2 from '../images/slide2.png';
import slide3 from '../images/slide3.png';


const Title = styled.h2 `
  color: #9da3d7;
  text-transform: uppercase;
  text-align: center;
  margin: 20px 0;
`;

const Block = styled.div `
  display: flex !important;
  height: 100%;
  background: #b1e1a3;
  align-items: center;
  justify-content: center;
`;

const SlideImg = styled.img `
  width: 100%;
  height: 100%;
`;


const SlickSlider = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
  };

  return (
    <div className='slider'>
      <Title> Тут у нас слайдер </Title>

      <Slider {...settings}>
        <Block>
          <SlideImg src={slide1} alt='slide_1' />
        </Block>

        <Block>
          <SlideImg src={slide2} alt='slide_2' />
        </Block>

        <Block>
          <SlideImg src={slide3} alt='slide_3' />
        </Block>
      </Slider>
    </div>
  );
};

export default SlickSlider;
