import React from 'react';
import styled from 'styled-components';


const Content = styled.div `
  max-width: 1000px;
  margin: 0 auto 60px;

  @media (max-width: 1000px) {
    padding: 0 50px;
  }
`;

const Title = styled.h2 `
  color: black;
  text-transform: uppercase;
  text-align: center;
  margin-bottom: 35px;
`;

const Text = styled.p `
  color: black;
  font-weight: 500;
  font-size: 14px;
`;


const TextBlock = () => {
  return (
    <Content>
      <Title> Просто текстовый блок </Title>

      <Text>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas consectetur, molestiae quam maxime incidunt ipsam magni vero minima ducimus ipsa corrupti unde culpa fugit id minus molestias fuga explicabo nihil? Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas consectetur, molestiae quam maxime incidunt ipsam magni vero minima ducimus ipsa corrupti unde culpa fugit id minus molestias fuga explicabo nihil?
      </Text>
    </Content>
  );
};

export default TextBlock;
