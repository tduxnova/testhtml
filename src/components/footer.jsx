import React from 'react';
import styled from 'styled-components';


const Box = styled.div `
  width: 100%;
  background: #777777;
`;

const Content = styled.div `
  max-width: 1000px;
  margin: 0 auto;

  @media (max-width: 1000px) {
    padding: 0 50px;
  }
`;

const Text = styled.p `
  color: white;
  font-size: 20px;
  width: 160px;
  padding: 35px 0;
`;


const Footer = () => {
  return (
    <Box>
      <Content>
        <Text> просто скромный футер </Text>
      </Content>
    </Box>
  );
};

export default Footer;
