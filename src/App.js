import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';


import reducer from './redux/reducer';
import Header from './components/header';
import Form from './components/form';
import Slider from './components/slider';
import TextBlock from './components/textBlock';
import Calculator from './components/calculator';
import Footer from './components/footer';


export const store = createStore(reducer, applyMiddleware(logger));


function App() {
  return (
    <Provider store={store}>
      <Header />
      <Form />
      <Slider />
      <TextBlock />
      <Calculator />
      <Footer />
    </Provider>
  );
}

export default App;
