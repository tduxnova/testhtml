export const makeWindowActive = (id) => ({
  type: 'MAKE_WINDOW_ACTIVE',
  payload: id
});
