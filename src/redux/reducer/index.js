export const initialState = {
  windows: [
    {
      id: 1,
      name: 'Одностворчатое',
      imgName: 'one-window.png',
      active: true,
      minWidth: '50px',
      minHeight: '90px',
      maxWidth: '95px',
      maxHeight: '160px',
      short: false,
      marginLeft: '35px',
    },
    {
      id: 2,
      name: 'Двухстворчатое',
      imgName: 'two-window.png',
      active: false,
      minWidth: '92px',
      minHeight: '90px',
      maxWidth: '120px',
      maxHeight: '160px',
      short: false,
      marginLeft: '20px',
    },
    {
      id: 3,
      name: 'Трехстворчатое',
      imgName: 'three-window.png',
      active: false,
      minWidth: '135px',
      minHeight: '90px',
      maxWidth: '175px',
      maxHeight: '160px',
      short: false,
    },
    {
      id: 4,
      name: 'Балконный блок',
      imgName: 'balcony-block.png',
      active: false,
      minWidth: '155px',
      minHeight: '135px',
      maxWidth: '175px',
      maxHeight: '160px',
      short: true,
    },
    {
      id: 5,
      name: 'Остекление балкона',
      imgName: 'balcony-glazing.png',
      active: false,
      minWidth: '155px',
      minHeight: '135px',
      maxWidth: '175px',
      maxHeight: '160px',
      short: true,
    },
  ],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case 'MAKE_WINDOW_ACTIVE':
      return { ...state, windows: state.windows.map((window) => {
          if (window.id === action.payload) {
            return { ...window, active: true };
          }

          return { ...window, active: false };
        })
      };

    default: 
      return state;
  }
};